# Tipo Scoring

## Set up project
    git clone git@gitlab.com:yasha.sudo/tipo_scoring.git
    cd tipo_scoring
    docker-compose build
    
## Run Django server
    docker-compose up -d
    
## Run tests
    docker-compose exec django pytest

    