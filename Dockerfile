FROM python:3.8

ENV PYTHONUNBUFFERED 1

RUN mkdir /src
WORKDIR /src
COPY . /src/
RUN pip install -U pip && pip install -U -r requirements.txt