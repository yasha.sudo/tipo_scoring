from pytest_factoryboy import register
from credit import factories

register(factories.CreditFactory)
register(factories.CustomerFactory)
