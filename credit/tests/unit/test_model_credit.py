import pytest
from credit import models
from django.forms.models import model_to_dict
from pprint import pprint


def test_credit_factory_returns_valid_credit(db, credit_factory):
    credit = credit_factory()
    assert isinstance(credit, models.Credit)
    assert credit.status == models.Credit.STATUS_CREATED


def test_credit_can_change_status_to_active(db, credit_factory):
    credit = credit_factory.active_credit()
    assert credit.status == models.Credit.STATUS_ACTIVE


def test_credit_can_change_status_to_paid(db, credit_factory):
    credit = credit_factory.paid_credit()
    assert credit.status == models.Credit.STATUS_PAID
