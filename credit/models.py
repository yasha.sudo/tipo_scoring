from django.db import models
from django_fsm import FSMField, transition


class Timestamp(models.Model):
    """ Abstract model for time fields: created & modified """

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Customer(Timestamp):
    """ Customer model"""

    iin = models.CharField(max_length=12, null=True, unique=True, verbose_name='ИИН')
    document_id = models.CharField(max_length=9, null=True, verbose_name='Номер документа')
    phone_number = models.CharField(max_length=10, unique=True, verbose_name='Номер телефона')

    first_name = models.CharField(max_length=32, null=True, verbose_name='Имя')
    last_name = models.CharField(max_length=32, null=True, verbose_name='Фамилия')
    middle_name = models.CharField(max_length=32, null=True, verbose_name='Отчество')


class Credit(Timestamp):
    """ Credit model """

    STATUS_CREATED = 'CREATED'
    STATUS_ACTIVE = 'ACTIVE'
    STATUS_PAID = 'PAID'

    STATUS_CHOICES = [
        (STATUS_CREATED, 'Created'),
        (STATUS_ACTIVE, 'Active'),
        (STATUS_PAID, 'Paid')
    ]

    MIN_AMOUNT = 5000
    MAX_AMOUNT = 100000

    MIN_DAYS = 5
    MAX_DAYS = 30

    status = FSMField(choices=STATUS_CHOICES, default=STATUS_CREATED, verbose_name='Статус кредита')
    amount = models.DecimalField(max_digits=len(str(MAX_AMOUNT)), decimal_places=0, null=True, verbose_name='Сумма')
    deadline = models.DateTimeField(null=True, verbose_name='Дата погашения')
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)

    @transition(field=status, source=STATUS_CREATED, target=STATUS_ACTIVE)
    def to_status_active(self):
        pass

    @transition(field=status, source=STATUS_ACTIVE, target=STATUS_PAID)
    def to_status_paid(self):
        pass
